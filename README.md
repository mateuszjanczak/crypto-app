![LOGO](./docs/logo.png)

Aplikacja powstała we współpracy z moim serdecznym kolegą Mateuszem jako projekt zaliczeniowy na studia.

## Wymagania
Node.js oraz npm można pobrać z [nodejs.org](https://nodejs.org)<br />
Uruchomiony serwer (back-end) z [bitbucket.org](https://bitbucket.org/Mateusze/crypto-service/src/master/)

## Podgląd
![PREVIEW](./docs/preview.gif)

## Skrypty

W folderze z projektem możesz użyć:

### `npm start`

Uruchamianie aplikacji w wersji developerskiej.<br />

### `npm run build`

Budowanie aplikacji w wersji produkcyjnej.<br />

### `npm run react-build`

Budowanie aplikacji React.<br />

### `npm run electron-build`

Budowanie aplikacji Electron.<br />

**Informacja: musisz najpierw zbudować aplikację React!**